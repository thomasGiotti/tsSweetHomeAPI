import * as morgan from "morgan";
import Logger from "../config/logger";

const stream: morgan.StreamOptions = {
  write: (message: string) => {
    Logger.info(message);
  },
};
const skip = () => {
  const env = process.env.NODE_ENV || "development";
  const isDevelopment = env === "development";
  return isDevelopment;
};

const morganMiddleware = morgan(
  ":method :url :status :res[content-length] - :response-time ms",
  { stream, skip }
);

export default morganMiddleware;
