function completeName(firstName: string, lastName: string): string {
    return firstName + " " + lastName
}

describe("test completeName function", () => {
    it("should return the complete name", () => {
        expect(completeName("Anthony", "Didier")).toBe("Anthony Didier");
    });
});