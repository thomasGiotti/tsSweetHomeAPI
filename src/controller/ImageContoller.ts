import { Router, Request, Response } from "express";
import { Image } from "../entity/Image"; 
import { ImageService } from "../services/ImageService"

class ImageController {
  public router: Router;
  private imageService: ImageService;
  constructor() {
    this.router = Router();
    this.imageService = new ImageService ();
    this.routes();
  }

  public findAll = (req: Request, res: Response) => {
    this.imageService.findAll().then((image) => {
      res.send(image);
    });
  };



  public findById = (req: Request, res: Response) => {
    const imageId = req["params"]["id"];
    this.imageService.findById(imageId).then((image) => {
      res.send(image);
    });
  };


  public create = (req: Request, res: Response) => {
    const imageCreate = req["body"] as Image;
    
    this.imageService.save(imageCreate).then((image) => {
      res.send(image);
    });
  };

  public update = (req: Request, res: Response) => {
    const imageUpdate = req["body"] as Image;
    const imageId = req["params"]["id"];
    this.imageService.update(imageId, imageUpdate).then((image) => {
      res.send(image);
    });
  };

  public delete = (req: Request, res: Response) => {
    const imageId = req["params"]["id"];
    this.imageService.delete(imageId).then(() => {
      res.sendStatus(200);
    });
  };

  public routes = () => {
    this.router.get("/", this.findAll);
    this.router.get("/:id", this.findById);
    this.router.post("/", this.create);
    this.router.put("/:id", this.update);
    this.router.delete("/:id", this.delete);
  };
}

export default ImageController;
