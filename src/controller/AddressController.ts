import { Router, Request, Response} from "express";
import { Address } from "../entity/Address";
import { AddressService } from "../services/AddressService";

class AddressController {
    public router: Router;
    private addressService: AddressService;

    constructor() {
        this.router = Router();
        this.addressService = new AddressService();
        this.routes();
    }

    public findAll = (req: Request, res: Response) => {
        this.addressService.findAll().then((address) => {
            res.send(address);
        });
    }

    public findById = (req: Request, res: Response, id: number) => {
        const addressId = req["params"]["id"];
        this.addressService.findById(addressId).then((address) => {
            res.send(address);
        });
    }

    public create = (req: Request, res: Response) => {
        const addressCreate =  req["body"] as Address;

        this.addressService.save(addressCreate).then((address) => {
            res.send(address);
        });
    }

    public update = (req:Request, res: Response) => {
        const addressUpdate = req["body"] as Address;
        const addressId = req["params"]["id"];
        this.addressService.update(addressId, addressUpdate).then((address) => {
            res.send(address);
        });
    }

    public delete = (req: Request, res: Response) => {
        const addressId = req["params"]["id"];
        this.addressService.delete(addressId).then(() => {
            res.sendStatus(200);
        });
    }

    public routes = () => {
        this.router.get("/", this.findAll);
        this.router.get("/:id", this.findById);
        this.router.post("/", this.create);
        this.router.put("/:id", this.update);
        this.router.delete("/:id", this.delete);
    };
}

export default AddressController;