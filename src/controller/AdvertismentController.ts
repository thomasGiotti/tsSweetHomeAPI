import { Router, Request, Response } from "express";
import { Advertisment } from "../entity/Advertisment";
import { AdvertismentService } from "../services/AdvertismentService"

class AdvertismentController {
  public router: Router;
  private advertismentService: AdvertismentService;
  constructor() {
    this.router = Router();
    this.advertismentService = new AdvertismentService();
    this.routes();
  }

  public findAll = (req: Request, res: Response) => {
    this.advertismentService.findAll().then((advertisment) => {
      res.send(advertisment);
    });
  };



  public findById = (req: Request, res: Response) => {
    const advertismentId = req["params"]["id"];
    this.advertismentService.findById(advertismentId).then((advertisment) => {
      res.send(advertisment);
    });
  };


  public create = (req: Request, res: Response) => {
    const avdretismentCreate = req["body"] as Advertisment;
    
    this.advertismentService.save(avdretismentCreate).then((advertisment) => {
      res.send(advertisment);
    });
  };

  public update = (req: Request, res: Response) => {
    const avdretismentUpdate = req["body"] as Advertisment;
    const advertismentId = req["params"]["id"];
    this.advertismentService.update(advertismentId, avdretismentUpdate).then((advertisment) => {
      res.send(advertisment);
    });
  };

  public delete = (req: Request, res: Response) => {
    const advertismentId = req["params"]["id"];
    this.advertismentService.delete(advertismentId).then(() => {
      res.sendStatus(200);
    });
  };

  public routes = () => {
    this.router.get("/", this.findAll);
    this.router.get("/:id", this.findById);
    this.router.post("/", this.create);
    this.router.put("/:id", this.update);
    this.router.delete("/:id", this.delete);
  };
}

export default AdvertismentController;
