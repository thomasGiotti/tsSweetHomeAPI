import { Router, Request, Response } from "express";
import { User } from "../entity/User";
import { UserService } from "../services/UserService";

class UserController {
  public router: Router;
  private userService: UserService;
  constructor() {
    this.router = Router();
    this.userService = new UserService();
    this.routes();
  }

  public findAll = (req: Request, res: Response) => {
    this.userService.findAll().then((users) => {
      res.send(users);
    });
  };

  public findById = (req: Request, res: Response) => {
    const userId = req["params"]["id"];
    this.userService.findById(userId).then((user) => {
      res.send(user);
    });
  };

  public update = (req: Request, res: Response) => {
    const userUpdate = req["body"] as User;
    const userId = req["params"]["id"];
    this.userService.update(userId, userUpdate).then((user) => {
      res.send(user);
    });
  };

  public delete = (req: Request, res: Response) => {
    const userId = req["params"]["id"];
    this.userService.delete(userId).then(() => {
      res.sendStatus(200);
    });
  };

  public routes = () => {
    this.router.get("/", this.findAll);
    this.router.get("/:id", this.findById);
    this.router.put("/:id", this.update);
    this.router.delete("/:id", this.delete);
  };
}

export default UserController;
