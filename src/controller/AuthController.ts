import { Request, Response, Router } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { validate } from "class-validator";

import { User } from "../entity/User";
import config from "../config/config";
import { UserService } from "../services/UserService";
import { AuthService } from "../services/AuthService";

class AuthController {
  public router: Router;
  private authService: AuthService;

  constructor() {
    this.router = Router();
    this.authService = new AuthService();
    this.routes();
  }

  public login = (req: Request, res: Response) => {
    const { email, password } = req.body;

    if (!(email && password)) {
      res.status(400).send();
    }
    this.authService
      .login(email, password)
      .then((user) => {
        console.log(user);
        res.send(user);
      })
      .catch((err) => {
        res.status(401).send(err);
      });
  };
  public register = (req: Request, res: Response) => {
    this.authService
      .register(req.body)
      .then((user) => {
        res.send(user);
      })
      .catch((err) => {
        console.log(err);
        res.status(400).send(err);
      });
  };

  public routes = () => {
    this.router.post("/login", this.login);
    /*     this.router.put("/change-password", this.changePassword);
     */ this.router.post("/logout", (req: Request, res: Response) => {
      res.sendStatus(204);
    });
    this.router.post("/register", this.register);
  };
}

export default AuthController;
