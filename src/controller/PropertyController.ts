import { Router, Request, Response } from "express";
import { Property } from '../entity/Property'
import { PropertyService } from '../services/PropertyService'

class PropertyController {
    public router: Router;
    private propertyService: PropertyService;
    constructor() {
        this.router = Router();
        this.propertyService = new PropertyService();
        this.routes();
    }

    public findAll = (req: Request, res: Response) => {
        this.propertyService.findAll().then((properties) => {
            res.send(properties);
        });
    };

    public findById = (req: Request, res: Response) => {
        const propertyId = req["params"]["id"];
        this.propertyService.findById(propertyId).then((property) => {
            res.send(property);
        });
    };

    public findByType = (req: Request, res: Response) => {
        const propertyType = req["params"]["type"];
        this.propertyService.findOneByType(propertyType).then((property) => {
            res.send(property);
        });
    };

    public create = (req: Request, res: Response) => {
        const propertyCreate = req["body"] as Property;
        this.propertyService.create(propertyCreate).then((property) => {
            res.send(property);
        });
    };

    public update = (req: Request, res: Response) => {
        const propertyUpdate = req["body"] as Property;
        const propertyId = req["params"]["id"];
        this.propertyService.update(propertyId, propertyUpdate).then((property) => {
            res.send(property);
        });
    };

    public delete = (req: Request, res: Response) => {
        const propertyId = req["params"]["id"];
        this.propertyService.delete(propertyId).then(() => {
            res.sendStatus(200);
        });
    };

    public routes = () => {
        this.router.get("/", this.findAll);
        this.router.get("/:id", this.findById);
        this.router.get("/:type", this.findByType);
        this.router.post("/", this.create);
        this.router.put("/:id", this.update);
        this.router.delete("/:id", this.delete);
    };
}

export default PropertyController;