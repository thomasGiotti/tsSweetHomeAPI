import { EntityRepository, Repository } from "typeorm";
import { Property } from "../entity/Property";

@EntityRepository(Property)
export class PropertyRepository extends Repository<Property> { }
