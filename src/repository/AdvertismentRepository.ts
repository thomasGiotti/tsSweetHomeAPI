import { EntityRepository, Repository } from "typeorm";
import { Advertisment } from "../entity/Advertisment";

@EntityRepository(Advertisment)
export class AdvertismentRepository extends Repository<Advertisment> {}
