import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getConnection, getCustomRepository, getRepository } from "typeorm";
import { validate } from "class-validator";

import { User } from "../entity/User";
import config from "../config/config";
import { UserRepository } from "../repository/UserRepository";

export class AuthService {
  private userRepository: UserRepository;

  constructor() {
    this.userRepository = getConnection().getCustomRepository(UserRepository);
  }

  public login = async (email: string, password: string) => {
    const user = await this.userRepository.findOne({
      where: {
        email: email,
      },
    });

    if (user && user.checkIfUnencryptedPasswordIsValid(password)) {
      const token = jwt.sign({ sub: user.id }, config.jwtSecret);

      return {
        user,
        token,
      };
    } else {
      throw new Error("Invalid email or password");
    }
  };

  public register = async (userToRegister: User) => {
    let user = await this.userRepository.findOne({
      where: {
        email: userToRegister.email,
      },
    });
    if (user) {
      throw new Error("User already exists");
    } else {
      user = new User();
      user.firstName = userToRegister.firstName;
      user.lastName = userToRegister.lastName;
      user.email = userToRegister.email;
      user.password = userToRegister.password;
      user.hashPassword();
      user.role = "user";
    }

    const errors = await validate(user);
    if (errors.length > 0) {
      throw new Error(errors.toString());
    }
    user = await this.userRepository.save(user);
    return user;
  };
}
