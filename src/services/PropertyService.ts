import { getConnection } from "typeorm";
import { Property } from "../entity/Property";
import { PropertyRepository } from "../repository/PropertyRepository";

export class PropertyService {
    private propertyRepository: PropertyRepository;

    constructor() {
        this.propertyRepository = getConnection().getRepository(Property);
    }

    public async findAll(): Promise<Property[]> {
        return await this.propertyRepository.find();
    }

    public async findById(id: number): Promise<Property> {
        return await this.propertyRepository.findOne(id);
    }

    public async findOneByType(type: string): Promise<Property> {
        return await this.propertyRepository.findOne({
            where: {
                type: type,
            },
        });
    }

    public async create(property: Property): Promise<Property> {
        return await this.propertyRepository.save(property);
    }

    public async update(id: number, property: Property): Promise<Property> {
        const propertyToUpdate = await this.propertyRepository.findOne(id);
        Object.assign(propertyToUpdate, property);
        return await this.propertyRepository.save(propertyToUpdate);
    }

    public async delete(id: number): Promise<void> {
        await this.propertyRepository.delete(id);
    }
}