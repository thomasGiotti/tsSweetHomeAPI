import { getConnection } from "typeorm";
import { Address } from "../entity/Address";
import { AddressRepository } from "../repository/AddressRepository";

export class AddressService {
    private adreessRepository: AddressRepository;

    constructor(){
        this.adreessRepository = getConnection().getRepository(Address);
    }

    public async findAll(): Promise<Address[]> {
        return await this.adreessRepository.find();
    }

    public async findById(id: number): Promise<Address> {
        return await this.adreessRepository.findOne(id)
    }

    public async findOneByemail(email: string): Promise<Address> {
        return await this.adreessRepository.findOne({
            where: {
                email: email,
            },
        })
    }

    public async save(address: Address): Promise<Address> {
        return await this.adreessRepository.save(address);
    }

    public async update(id: number, address:Address): Promise<Address> {
        const addressToUpdate = await this.adreessRepository.findOne(id);
        Object.assign(addressToUpdate,address);
        return await this.adreessRepository.save(addressToUpdate);
    }

    public async delete(id: number): Promise<void> {
        await this.adreessRepository.delete(id);
    }
} 