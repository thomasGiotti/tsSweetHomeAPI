import { getConnection } from "typeorm";
import { User } from "../entity/User";
import { UserRepository } from "../repository/UserRepository";

export class UserService {
  private userRepository: UserRepository;

  constructor() {
    this.userRepository = getConnection().getRepository(User);
  }

  public async findAll(): Promise<User[]> {
    return await this.userRepository.find();
  }

  public async findById(id: number): Promise<User> {
    return await this.userRepository.findOne(id);
  }

  public async findOneByEmail(email: string): Promise<User> {
    return await this.userRepository.findOne({
      where: {
        email: email,
      },
    });
  }

  public async update(id: number, user: User): Promise<User> {
    const userToUpdate = await this.userRepository.findOne(id);
    Object.assign(userToUpdate, user);
    return await this.userRepository.save(userToUpdate);
  }

  public async delete(id: number): Promise<void> {
    await this.userRepository.delete(id);
  }
}
