import { getConnection } from "typeorm";
import { Image } from "../entity/Image"; 
import { ImageRepository } from "../repository/ImageRepository";

export class ImageService {
  private imageRepository: ImageRepository;

  constructor() {
    this.imageRepository= getConnection().getRepository(Image);
  }

  public async findAll(): Promise<Image[]> {
    return await this.imageRepository.find();
  }

  public async findById(id: number): Promise<Image> {
    return await this.imageRepository.findOne(id);
  }

//   public async findOneByemail(email: string): Promise<Image> {
//     return await this.imageRepository.findOne({
//       where: {
//         email: email,
//       },
//     });
//   }

  public async save(image: Image): Promise<Image> {
    return await this.imageRepository.save(image);
  }


  public async update(id: number, image: Image): Promise<Image> {
    const imageToUpdate = await this.imageRepository.findOne(id);
    Object.assign(imageToUpdate, image);
    return await this.imageRepository.save(imageToUpdate);
  }

  public async delete(id: number): Promise<void> {
    await this.imageRepository.delete(id);
  }
}
