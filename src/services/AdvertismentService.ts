import { getConnection } from "typeorm";
import { Advertisment } from "../entity/Advertisment";
import { AdvertismentRepository } from "../repository/AdvertismentRepository";

export class AdvertismentService {
  private advertismentRepository: AdvertismentRepository;

  constructor() {
    this.advertismentRepository= getConnection().getRepository(Advertisment);
  }

  public async findAll(): Promise<Advertisment[]> {
    return await this.advertismentRepository.find();
  }

  public async findById(id: number): Promise<Advertisment> {
    return await this.advertismentRepository.findOne(id);
  }

  public async findOneByemail(email: string): Promise<Advertisment> {
    return await this.advertismentRepository.findOne({
      where: {
        email: email,
      },
    });
  }

  public async save(advertisment: Advertisment): Promise<Advertisment> {
    return await this.advertismentRepository.save(advertisment);
  }


  public async update(id: number, advertisment: Advertisment): Promise<Advertisment> {
    const advertismentToUpdate = await this.advertismentRepository.findOne(id);
    Object.assign(advertismentToUpdate, advertisment);
    return await this.advertismentRepository.save(advertismentToUpdate);
  }

  public async delete(id: number): Promise<void> {
    await this.advertismentRepository.delete(id);
  }
}
