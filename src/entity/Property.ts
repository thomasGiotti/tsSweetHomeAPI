import { Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn, ManyToOne, OneToOne } from "typeorm";
import { Image } from "./Image";
import { Address } from "./Address";
import { Advertisment } from "./Advertisment";
import { User } from "./User";

export enum property_type {
    appartement = "Appartement",
    pavillon = "Pavillon",
    terrain = "Terrain",
    parking_box = "Parking box",
    loft_atelier = "Loft atelier",
    villa = "Villa",
    chateau = "Château",
    hotel_particulier = "Hôtel particulier",
    batiment = "Bâtiment",
    peniche = "Péniche",
    autre = "Autre",
}

export enum purchase_type {
    ancien = "Ancien",
    neuf = "Neuf",
    viager = "Viager",
    projet_construction = "Projet de construction",
}

export enum convenience_type {
    parking_ouvert = "Parking ouvert",
    box = "Box",
    parquet = "Parquet",
    dernier_etage = "Dernier étage",
    placard = "Placard",
    acces_handicape = "Accès handicapé",
    alarme = "Alarme",
}

export enum room_type {
    toilettes_separees = "Toilettes séparées",
    entree_separee = "Entrée séparée",
    salle_de_bain = "Salle de bain",
    salle_d_eau = "Salle d'eau",
    sejour = "Séjour",
    salle_a_manger = "Salle à manger",
}

export enum kitchen_type {
    cuisine_separee = "Cuisine séparée",
    cuisine_americaine = "Cuisine américaine",
    coin_cuisine = "Coin cuisine",
    cuisine_equipee = "Cuisine équipée",
}

export enum heating_type {
    individuel = "Chauffage individuel",
    central = "Chauffage central",
    electrique = "Chauffage électrique",
    gaz = "Chauffage au gaz",
    sol = "Chauffage au sol",
}

export enum exposure_type {
    sans_vis_a_vis = "Sans vis-à-vis",
    sud = "Plein sud",
}

@Entity()
export class Property {
    @PrimaryGeneratedColumn("identity")
    id: number;

    @Column()
    area: number;

    @Column()
    nb_rooms: number;

    @Column()
    type: property_type;

    @Column({ nullable: false, type: "float", default: 0.0 })
    price: number;

    @Column()
    nb_bedrooms: number;

    @Column()
    purchase_type: purchase_type;

    @Column()
    convenience: convenience_type;

    @Column()
    other_room: room_type;

    @Column()
    kitchen: kitchen_type;

    @Column()
    heating: heating_type;

    @Column()
    exposure: exposure_type;

    @OneToMany(() => Image, image => image.property)
    images: Image[];

    @OneToOne(() => Advertisment, { onDelete: "CASCADE" })
    advertisment: Advertisment;

    @OneToOne(() => Address, { onDelete: "CASCADE" })
    @JoinColumn()
    address: Address;

    @ManyToOne(() => User, (user) => user.properties)
    user: User;
}

export default Property;
