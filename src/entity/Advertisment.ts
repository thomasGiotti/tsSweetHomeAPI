import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  OneToOne,
  JoinColumn,
  ManyToOne,
} from "typeorm";
import Property from "./Property";
import { User } from "./User";

@Entity()
export class Advertisment {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    default: "false",
  })
  verified: boolean;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToOne(() => Property, (property) => property.advertisment, {
    onDelete: "CASCADE",
  })
  @JoinColumn()
  property: Property;

  @ManyToOne(() => User, (user) => user.advertisments, {
    onDelete: "CASCADE",
  })
  user: User;
}
