import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import { Property } from "./Property"

@Entity()
export class Image {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    media: string;

    @ManyToOne(() => Property, property => property.images, {
    
        onDelete: "CASCADE"
        
    })
    property: Property;
}
