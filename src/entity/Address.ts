import {Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn} from "typeorm";
import Property from "./Property";

@Entity()
export class Address {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    country: string;

    @Column()
    province: string;

    @Column()
    city: string;

    @Column()
    street: string;

    @Column()
    street_number: string;

    @Column()
    post_code: string;

    @Column()
    lat: string;

    @Column()
    lng: string;

    @OneToOne(() => Property, {onDelete: "CASCADE"})
    property: Property
}

