import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import helmet from "helmet";
import * as cors from "cors";
import UserController from "./controller/UserController";
import AuthController from "./controller/AuthController";
import ImageController from "./controller/ImageContoller";
import PropertyController from "./controller/PropertyController";
import AdvertismentController from "./controller/AdvertismentController";
import AddressController from "./controller/addressController";
import { checkJwt } from "./middlewares/checkJwt";
import morganMiddleware from "./middlewares/morganMiddleware";
import Logger from "./config/logger";

class Server {
  private app: express.Application;
  private userController: UserController;
  private authController: AuthController;
  private propertyController: PropertyController;
  private addressController: AddressController;
  private advertismentController: AdvertismentController;
  private imageController: ImageController;
  constructor() {
    this.app = express();
    this.config();
    this.routes();
  }

  public config() {
    this.app.set("port", process.env.PORT || 3000);
  }

  public async routes() {
    await createConnection();
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(helmet());
    this.app.use(cors());
    this.app.get("/", (req, res) => {
      res.send("Hello World!");
    });
    this.app.use(morganMiddleware);
    this.userController = new UserController();
    this.authController = new AuthController();
    this.propertyController = new PropertyController();
    this.advertismentController = new AdvertismentController();
    this.imageController = new ImageController();
    this.addressController = new AddressController();
    this.app.use("/api/user", checkJwt, this.userController.router);
    this.app.use("/api/auth", this.authController.router);

    this.app.get("/logger", (_, res) => {
      Logger.error("This is an error log");
      Logger.warn("This is a warn log");
      Logger.info("This is a info log");
      Logger.http("This is a http log");
      Logger.debug("This is a debug log");

      res.send("Hello world");
    });
    this.app.use("/api/property", this.propertyController.router);
    this.app.use("/api/advertisment", this.advertismentController.router);
    this.app.use("/api/image", this.imageController.router);
    this.app.use("/api/address", this.addressController.router);
  }

  public async start() {
    await createConnection();
    console.log("first");
    this.app.listen(this.app.get("port"), () => {
      console.log(
        `Server is running on http://localhost:${this.app.get("port")}`
      );
    });
  }
}

const server = new Server();
server.start();
